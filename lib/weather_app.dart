import 'package:flutter/material.dart';

class WeatherApp extends StatelessWidget {
  const WeatherApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        // appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }
}

AppBar buildAppBarWidget(BuildContext context) {
  return AppBar(
    backgroundColor: Colors.grey.shade600,
    elevation: 0,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.black54,
      size: 30.0,
    ),
    title: Text(
      "Weather App",
      style: TextStyle(
        color: Colors.white,
        fontWeight: FontWeight.w400,
        fontSize: 25.0,
      ),
    ),
  );
}

Widget buildBodyWidget() {
  return Stack(
    children: <Widget>[
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://media.nationalgeographic.org/assets/interactives/363/5bc/3635bc3e-7c8b-4c88-a26a-ddbd7882688e/public/splash/images/splash_bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: ListView(
          children: <Widget>[
            Container(
              height: 210,
              margin: EdgeInsets.all(20),
              child: buildCurrentTemp(),
            ),
            Container(
              height: 500.0,
              margin: EdgeInsets.all(30),
              child: buildDayForecast(),
            ),
          ],
        ),
      ),
    ],
  );
}

Widget buildCurrentTemp() {
  return ListView(
    children: <Widget>[
      Text(
        "Amphoe Bang Bo",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w400,
          fontSize: 40.0,
        ),
        textAlign: TextAlign.center,
      ),
      Text(
        "25°",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w300,
          fontSize: 90.0,
        ),
        textAlign: TextAlign.center,
      ),
      Text(
        "Partly Cloudy",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w400,
          fontSize: 25.0,
        ),
        textAlign: TextAlign.center,
      ),
      Text(
        "H:31° L:22°",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w400,
          fontSize: 25.0,
        ),
        textAlign: TextAlign.center,
      ),
    ],
  );
}

Widget buildDayForecast() {
  return ListView(
    children: <Widget>[
      day_Forecast(),
      Divider(
        color: Colors.white,
        thickness: 1.5,
      ),
      day_Today(),
      Divider(
        color: Colors.white,
        thickness: 1,
      ),
      day_1(),
      Divider(
        color: Colors.white,
        thickness: 1,
      ),
      day_2(),
      Divider(
        color: Colors.white,
        thickness: 1,
      ),
      day_3(),
      Divider(
        color: Colors.white,
        thickness: 1,
      ),
      day_4(),
      Divider(
        color: Colors.white,
        thickness: 1,
      ),
      day_5(),
      Divider(
        color: Colors.white,
        thickness: 1,
      ),
      day_6(),
      Divider(
        color: Colors.white,
        thickness: 1,
      ),
      day_7(),
      Divider(
        color: Colors.white,
        thickness: 1,
      ),
      day_8(),
      Divider(
        color: Colors.white,
        thickness: 1,
      ),
      day_9(),
      Divider(
        color: Colors.white,
        thickness: 1,
      ),
    ],
  );
}

Widget day_Forecast() {
  return Container(
      margin: EdgeInsets.only(top: 5),
      padding: EdgeInsets.all(5),
      child: Row(
        children: [
          Icon(
            Icons.calendar_month,
            color: Colors.white,
            size: 30,
          ),
          Text(
            '  10 - DAY FORECAST',
            style: TextStyle(
              color: Colors.white,
              fontSize: 30.0,
            ),
          ),
        ],
      ));
}

Widget day_Today() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        "Today",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Text(
        "22°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Icon(
        Icons.cloud,
        color: Colors.white,
        size: 30.0,
      ),
      Text(
        "31°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
    ],
  );
}

Widget day_1() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        "Wed",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Text(
        "23°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Icon(
        Icons.sunny,
        color: Colors.white,
        size: 30.0,
      ),
      Text(
        "32°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
    ],
  );
}

Widget day_2() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        "Thu",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Text(
        "23°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Icon(
        Icons.sunny,
        color: Colors.white,
        size: 30.0,
      ),
      Text(
        "32°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
    ],
  );
}

Widget day_3() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        "Fri",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Text(
        "23°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Icon(
        Icons.cloud,
        color: Colors.white,
        size: 30.0,
      ),
      Text(
        "29°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
    ],
  );
}

Widget day_4() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        "Sat",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Text(
        "22°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Icon(
        Icons.cloud,
        color: Colors.white,
        size: 30.0,
      ),
      Text(
        "26°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
    ],
  );
}

Widget day_5() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        "Sun",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Text(
        "22°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Icon(
        Icons.cloud,
        color: Colors.white,
        size: 30.0,
      ),
      Text(
        "28°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
    ],
  );
}

Widget day_6() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        "Mon",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Text(
        "23°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Icon(
        Icons.cloudy_snowing,
        color: Colors.white,
        size: 30.0,
      ),
      Text(
        "31°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
    ],
  );
}

Widget day_7() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        "Tue",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Text(
        "24°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Icon(
        Icons.cloud,
        color: Colors.white,
        size: 30.0,
      ),
      Text(
        "32°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
    ],
  );
}

Widget day_8() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        "Wed",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Text(
        "24°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Icon(
        Icons.sunny,
        color: Colors.white,
        size: 30.0,
      ),
      Text(
        "32°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
    ],
  );
}

Widget day_9() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        "Thu",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Text(
        "24°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
      Icon(
        Icons.sunny,
        color: Colors.white,
        size: 30.0,
      ),
      Text(
        "32°",
        style: TextStyle(
          fontWeight: FontWeight.w400,
          color: Colors.white,
          fontSize: 25.0,
        ),
      ),
    ],
  );
}

