import 'package:flutter/material.dart';

// class AspectRatioExample extends StatelessWidget {
//   const AspectRatioExample({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         body: Center(
//           child: SizedBox(
//             width: 300.0,
//             child: AspectRatio(
//               aspectRatio: 1.5,
//               child: Container(
//                 color: Colors.green[200],
//                 child: const FlutterLogo(),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

class AspectRatioExample extends StatelessWidget {
  const AspectRatioExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(50.0),
      child: Center(
          child: AspectRatio(
            aspectRatio: 1.5,
            child: Container(
              color: Colors.green[200],
              child: const FlutterLogo(),
            ),
          ),
      ),
    );
  }
}
